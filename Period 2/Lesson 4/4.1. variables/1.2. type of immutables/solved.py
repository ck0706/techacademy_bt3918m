# numbers

# integer
my_integer = 10
print("type of my_integer variable is:", type(my_integer))

# float
my_float = 102.65
print("type of my_float variable is:", type(my_float))

# complex
my_complex = 12j
print("type of my_complex variable is:", type(my_complex))

# strings
my_string_1 = 'birinci sətir'
print("type of my_string_1 variable is:", type(my_string_1))

my_string_2 = "ikinci sətir"
print("type of my_string_2 variable is:", type(my_string_2))

my_string_3 = '''üçüncü sətir'''
print("type of my_string_3 variable is:", type(my_string_3))

my_string_4 = """dördüncü sətir"""
print("type of my_string_4 variable is:", type(my_string_4))

# tuples
my_tuple = (1, 2, 205, 16.436, "salam dünya!")
print("type of my_tuple variable is:", type(my_tuple))

